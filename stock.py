# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields
from trytond.transaction import Transaction
from trytond.pool import Pool
from decimal import Decimal

__all__ = ['Move']


class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'

    def get_supplier(self, name):
        supplier = super(Move, self).get_supplier(name)
        return supplier if supplier else getattr(
            self.shipment, 'supplier', None)

    def _get_context_purchase_price(self):
        context = {}
        supplier = self.get_supplier('supplier')
        if supplier:
            context['supplier'] = supplier.id
        if self.uom:
            context['uom'] = self.uom.id
        if self.product.product_suppliers:
            # TODO: should be selected by user
            product_supplier = [p for p in self.product.product_suppliers
                if p.party == supplier]
            if product_supplier:
                context['product_supplier'] = product_supplier[0]
        return context

    @fields.depends('product', 'quantity', 'uom', 'unit', 'unit_price',
        'purchase', 'purchase_currency', 'supplier', 'shipment',
        '_parent_product.product_suppliers', 'origin', 'from_location')
    def on_change_quantity(self):
        Product = Pool().get('product.product')

        if (self.shipment and self.product and
                self.shipment.__name__ == 'stock.shipment.in' and
                self.from_location and
                self.from_location.type == 'supplier' and
                not self.origin):
            with Transaction().set_context(self._get_context_purchase_price()):
                self.unit_price = Product.get_purchase_price([self.product],
                    abs(self.quantity or 0))[self.product.id]
                if self.unit_price:
                    self.unit_price = self.unit_price.quantize(
                        Decimal(1) / 10 ** self.__class__.unit_price.digits[1])
        elif hasattr(super(Move, self), 'on_change_quantity'):
            super(Move, self).on_change_quantity()
