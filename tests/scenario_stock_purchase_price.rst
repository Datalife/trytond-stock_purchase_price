=============================
Stock Purchase Price Scenario
=============================

Imports::

    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard, Report
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> today = datetime.date.today()
    >>> yesterday = today - relativedelta(days=1)

Install stock_purchase_price::

    >>> config = activate_modules('stock_purchase_price')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> payable = accounts['payable']
    >>> receivable = accounts['receivable']

Create customer & supplier::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()
    >>> supplier = Party(name='Supplier', account_receivable=receivable.id, account_payable=payable.id)
    >>> supplier.save()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('20')
    >>> template.save()
    >>> product, = template.products
    >>> product.cost_price = Decimal('10')
    >>> product.save()

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> supplier_loc, = Location.find([('code', '=', 'SUP')])
    >>> customer_loc, = Location.find([('code', '=', 'CUS')])
    >>> input_loc, = Location.find([('code', '=', 'IN')])
    >>> output_loc, = Location.find([('code', '=', 'OUT')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])
    >>> lost_loc, = Location.find([('type', '=', 'lost_found')])

Create Shipment In::

    >>> ShipmentIn = Model.get('stock.shipment.in')
    >>> shipment_in = ShipmentIn()
    >>> shipment_in.planned_date = today
    >>> shipment_in.supplier = supplier
    >>> shipment_in.warehouse = warehouse_loc
    >>> shipment_in.company = company

Create product supplier::

    >>> ProductSupplier = Model.get('purchase.product_supplier')
    >>> Price = Model.get('purchase.product_supplier.price')
    >>> prices = [Price(sequence=1, quantity=10, unit_price=Decimal('5'))]
    >>> product_supplier = ProductSupplier(product=template.id, party=supplier.id, prices=prices)
    >>> product_supplier.save()

Create move::

    >>> move = shipment_in.incoming_moves.new()
    >>> move.from_location = supplier_loc
    >>> move.to_location = input_loc
    >>> move.product = product
    >>> move.uom = unit
    >>> move.quantity = 1
    >>> move.company = company
    >>> move.currency = company.currency
    >>> shipment_in.save()

When quantity is increased discount is applied::

    >>> move.unit_price
    Decimal('10')
    >>> move.quantity = 10
    >>> move.unit_price
    Decimal('5.0000')
