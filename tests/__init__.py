# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from .test_stock_purchase_price import suite

__all__ = ['suite']
