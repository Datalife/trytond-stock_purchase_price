# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import stock


def register():
    Pool.register(
        stock.Move,
        module='stock_purchase_price', type_='model', depends=['stock'])
    Pool.register(
        module='stock_purchase_price', type_='wizard')
    Pool.register(
        module='stock_purchase_price', type_='report')
